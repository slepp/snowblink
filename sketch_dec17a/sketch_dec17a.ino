
/*
 * SnowBlink.c
 *
 * Created: 16/12/2014 6:38:06 PM
 *  Author: slepp
 */ 


#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

typedef enum { OFF, SQUARE, TRIANGLE, SHARP_TRIANGLE } animations;

static inline void powerDown() {
	// Power-down mode
	SMCR = (1<<SM1);
	asm("SLEEP");
}

void stopTimer() {
	PRR |= (1<<PRTIM0);
}

void startTimer() {
	PRR &= ~(1<<PRTIM0);
}

EMPTY_INTERRUPT(WDT_vect);
//ISR(WDT_vect) {
//}

static inline uint8_t rnd(void) {
	static uint8_t s=0xaa,a=0;

    s^=s<<3;
    s^=s>>5;
    s^=a++>>2;
    return s;
}

struct ledState {
	animations state;
	uint8_t step;
	uint8_t duty;
	uint8_t nextStepTicks;
        uint8_t stepRate;
        uint8_t peak;
        uint8_t halfPeak;
	} ledState[3];

int linearPWM(int percentage){
// coefficients
double a = 9.7758463166360387E-01;
double b = 5.5498961535023345E-02;
return floor((a * exp(b*(percentage/2.55))+.5))-1;
}

void sequenceLED(const uint8_t ledNumber) {
	struct ledState *led = &ledState[ledNumber];
	if(--led->nextStepTicks == 0) { // Tick along
		++led->step;
		switch(led->state) {
			case OFF:
			  led->duty = 0;
			  if(rnd() > 200) {
				  register uint8_t rand = rnd() & 0x1;
				  if(rand == 0)
				    led->state = OFF;
				  else if(rand == 1) {
				    led->state = SQUARE;
                                    led->stepRate = (rnd() & 0x7) + 8;
                                    led->peak = (rnd() & 31) + 32;
                                    led->halfPeak = led->peak >> 1;
                                    led->duty = 0;
                                    led->step = 0;
				  } else if(rand == 2)
				    led->state = TRIANGLE;
				  else if(rand == 3)
				    led->state = SHARP_TRIANGLE;
				  led->nextStepTicks = 1;
			  } else {
			    led->nextStepTicks = 100;
			    led->step = 0;
			  }
			  break;
			case SQUARE:
			  if(led->step > led->halfPeak) // Down ramp first
			    --led->duty; // = led->peak - (led->step-led->halfPeak);
			  else
			    ++led->duty; // = led->step;
                          if(led->step == led->halfPeak)
                             led->stepRate = (rnd() & 0x7) + 8;
			  led->nextStepTicks = led->stepRate;
			  if(led->step == led->peak) {
				  led->duty = 0;
				  led->state = OFF;
			  }
			  break;
			case TRIANGLE:
			  if(led->step > 127) // Down ramp first
			    led->duty = 255 - ((led->step-128) * 2);
			  else
			    led->duty = led->step * 2;
			  led->nextStepTicks = 3;
			  if(led->step == 255) {
				  led->duty = 0;
				  led->state = OFF;
			  }
			  break;
			case SHARP_TRIANGLE:
			  if(led->step > 63) // Down ramp first
			    led->duty = 255 - ((led->step-64) * 4);
			  else
			    led->duty = led->step * 4;
			  led->nextStepTicks = 3;
			  if(led->step == 127) {
				led->duty = 0;
				led->state = OFF;
			  }
			  break;
		}
		switch(ledNumber) {
			case 0: analogWrite(9,led->duty); break;
			case 1: analogWrite(10,led->duty); break;
			case 2: analogWrite(11,led->duty); break;
		}
	}
}

volatile uint16_t tick = 0;
volatile uint8_t ticked = 0;

ISR(TIM0_OVF_vect) {
	ticked = 1;
	++tick;
}

void setup(void)
{
	// Set clock to 128kHz
	//CCP = 0xD8;
	//CLKMSR = 0b01;
	
	// Setup for lowest power
	//PORTCR = 0; // Disable make before break
	//PUEB = 0; // Disable pull ups
	DDRB = 0xFF; // All ports output
	PORTB = 0; // Register low

	ledState[0].nextStepTicks = 1;
	ledState[1].nextStepTicks = 1;
	ledState[2].nextStepTicks = 1;

	/*TCCR0A = _BV(COM0A1) | _BV(COM0B1) | // Clear up, set down
			 _BV(WGM00); // Phase correct, 8 bit
	TCCR0B |= _BV(CS00); // IOclk/1
	TCCR0C = 0; // Set 0, may not be needed
	TIMSK0 = _BV(TOIE0); // Interrupt on overflow, this is our "clock"*/
	
	// Enable watchdog to 8 seconds
	WDTCSR = (1<<WDE) | (1<<WDIE) |
			 (1<<WDP3) | (1<<WDP0);

	// 8bit PWM
	//OCR0AH = 0;
	//OCR0BH = 0;

    while(1)
    {
		asm("WDR"); // Clear watchdog
                delay(1);
		if(1 || ticked) {
			sequenceLED(0);
			sequenceLED(1);
			sequenceLED(2);
			ticked = 0;
		}
		powerDown();
		/*if(tick == 0) {
			ledMode[0] = rnd() & 0x3;
			ledMode[1] = rnd() & 0x3;
			ledMode[2] = rnd() & 0x3;
		}*/
    }
}

void loop() {
}
