/*
 * SnowBlink.c
 *
 * Created: 16/12/2014 6:38:06 PM
 *  Author: slepp
 */ 

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

struct statusFlags {
	uint8_t ticked : 1;
	uint8_t softTicked : 1;
	uint8_t fellAsleep : 1;
	uint8_t wdtSampled : 1;
};
static volatile struct statusFlags status;
static uint8_t randSeed = 0;

uint8_t rnd(void) {
	static uint8_t a=0;

    randSeed^=randSeed<<3;
    randSeed^=randSeed>>5;
    randSeed^=a++>>2;
    return randSeed;
}

struct ledState {
	uint8_t step;
	uint8_t nextStepTicks;
	uint8_t stepRate; // Double duty as a "LED is on" flag, if 0, it's off.
	//uint8_t duty;
	uint8_t halfPeak;
} ledState[3];

#define ATTACK ((rnd() & 7) + 8)
#define DECAY  ((rnd() & 7) + 8)
#define PEAK   ((rnd() & 31) + 32)

#define SOFT_ATTACK ((rnd() & 4) + 3)
#define SOFT_DECAY  ((rnd() & 4) + 3)
#define SOFT_PEAK   ((rnd() & 15) + 8)

volatile uint8_t softPwmBuffer = 0;

void sequenceLED(const uint8_t ledNumber) {
	struct ledState *led = &(ledState[ledNumber]);
	uint8_t duty;
	// Load the current duty
	if(ledNumber == 0)
	  duty = OCR0AL;
	else if(ledNumber == 1)
	  duty = OCR0BL;
	else
	  duty = softPwmBuffer;
	  
	// Is it time for the next step?
	if(--led->nextStepTicks == 0) { // Tick along
		// If we're off, see if we should turn on.
		if(led->stepRate == 0) {
			//duty = 0;
			uint8_t rand = rnd();
			if((status.fellAsleep /*&& rand > 20*/) || (rand > 250 && ledNumber<2) || (rand > 60 && ledNumber == 2)) {
				status.fellAsleep = 0;
				led->stepRate = (ledNumber != 2 ? ATTACK : SOFT_ATTACK);
				led->halfPeak = (ledNumber != 2 ? PEAK : SOFT_PEAK);
				//duty = 0;
				led->step = 0;
			} else {
				led->nextStepTicks = 100;
			}
		} else {
			++led->step;
			// Adjust the triangle output up or down
			if(led->step > led->halfPeak) { // Down ramp first
				if(duty > 0)
				  --duty;
			} else {
				if(duty < ICR0L)
				  ++duty;
			}
			
			// If we're at our halfway mark, adjust decay rate
			if(led->step == led->halfPeak) {
				if(ledNumber == 2)
				  led->stepRate = SOFT_DECAY;
				 else
				  led->stepRate = DECAY;
			}
			
			// Set the wait time until the next step
			led->nextStepTicks = led->stepRate;
			
			// If we're at the top, stop.
			if(led->step >= (uint8_t)(led->halfPeak << 1)) {
				duty = 0;
				led->stepRate = 0;
				led->nextStepTicks = 100;
			}
		}
		
		if(ledNumber == 0)
			OCR0AL = duty;
		else if(ledNumber == 1)
			OCR0BL = duty;
		else if(ledNumber == 2)
			softPwmBuffer = duty;
	}
}

// If the WDT triggers, count that as a tick
//EMPTY_INTERRUPT(WDT_vect);
ISR(WDT_vect) {
	status.wdtSampled = 1;
}

#define LED3_OFF() portBitSet = 0
#define LED3_ON() portBitSet = _BV(PORTB2)

volatile uint8_t portBitSet = 0;
ISR(TIM0_OVF_vect) {
	static uint8_t softPwm = 0;
	static uint8_t tick = 0;

	// PWM isn't overridden by port writes?
	PORTB = portBitSet;
	
	// Ticked flag
	++tick;
	tick &= 31;
    status.ticked = 1;
	// Count up, update the software PWM threshold
	if(tick == 0) {
		LED3_ON();
		softPwm = softPwmBuffer;
		status.softTicked = 1;
	}

	if(tick == softPwm) {
		LED3_OFF();
	}
}

uint8_t rotl(const uint8_t value, uint8_t shift) {
  if ((shift &= sizeof(value)*8 - 1) == 0)
    return value;
  return (value << shift) | (value >> (sizeof(value)*8 - shift));
}

#define SET_WDIE() 	\
	asm volatile( \
		"in __tmp_reg__,0x31\n\t" \
		"ori __tmp_reg__,0x40\n\t" \
		"out 0x3C,%0\n\t" \
		"out 0x31,__tmp_reg__\n\t" \
		: \
		: "r" ((uint8_t)0xD8) \
	)


int main(void)
{
	// Ensure the clock sequencing for adjustments after CCP
	// Setup master clock to be the internal 8MHz divided by CLKPSR
	// Setup WDT to 125ms
	asm volatile(
		"LDI __tmp_reg__, 0xD8\n\t"
		"OUT __CCP__, __tmp_reg__\n\t" // 2 instructions per CCP write
		"OUT %0, %A1\n\t"
		"OUT %2, %A3\n\t"
		"OUT __CCP__, __tmp_reg__\n\t"
		"OUT %4, %A5\n\t"
		"OUT %6, __zero_reg__\n\t"
		:
		: "I" (_SFR_IO_ADDR(CLKMSR)), "r" ((uint8_t)0)
		, "I" (_SFR_IO_ADDR(CLKPSR)), "r" ((uint8_t)0b11)
		, "I" (_SFR_IO_ADDR(WDTCSR)), "r" ((uint8_t)((1<<WDE)|(1<<WDIE)|(1<<WDP1)|(1<<WDP0)))
		, "I" (_SFR_IO_ADDR(RSTFLR))
	);

	// Setup for lowest power and LED outputs
	DDRB = 0x7; // PB0:2 ports are output
	PUEB = 0; // Disable pull ups
	PORTB = 0; // Bring everything low

	for(uint8_t i = 0; i < 3; i++) {
		/*asm volatile(
			"st %a1+,%0\n\t" 			// nextStepTicks
			"st %a1+,__zero_reg__\n\t"	// stepRate
			"st %a1,__zero_reg__\n\t"	// duty
			:: "r" ((uint8_t)1), "e" (&ledState[i].nextStepTicks));*/
		ledState[i].nextStepTicks = 1;
		ledState[i].stepRate = 0;
		//ledState[i].duty = 0;
	}

	TCCR0A = _BV(COM0A1) | // Clear up, set down (non-inverted)
			 _BV(COM0B1) |
			 _BV(WGM01); // Phase correct, 8 bit
	TCCR0B |= _BV(WGM03) | _BV(CS00); // IOclk/1
	TIMSK0 = _BV(TOIE0); // Interrupt on overflow, this is our "clock"
	ICR0 = 250; // Near an "even" interval

	// 8bit PWM
	OCR0AH = 0;
	OCR0BH = 0;

	// Enable all interrupts
	sei();
	
	// Time to seed the RNG
#if 1
	{
		CCP = 0xD8;
		WDTCSR = (1<<WDE) | (1<<WDIE);
		set_sleep_mode(SLEEP_MODE_IDLE);
		uint8_t i = 0;
		while(i!=64) {
			sleep_mode();
			SET_WDIE();
			if(status.wdtSampled) {
				status.wdtSampled = 0;
				randSeed = rotl(randSeed, 1);
				randSeed ^= TCNT0L;
				i++;
			}
		}
		CCP = 0xD8;
		WDTCSR = (1<<WDE) | (1<<WDIE) | (0<<WDP2) | (1<<WDP1) | (1<<WDP0);
	}
#endif

	// Clear our status flags out
	status.ticked = 0;
	status.fellAsleep = 0;
	status.wdtSampled = 0;
	
    while(1)
    {
		wdt_reset();
		cli();
		if(status.ticked) {
			status.ticked = 0;
			sequenceLED(0);
			sequenceLED(1);
		}
		if(status.softTicked) {
			status.softTicked = 0;
			sequenceLED(2);
		}
		sei();
		// Cheat a little: If all the stepRates ORed together == 0, none are active
		if((ledState[0].stepRate | ledState[1].stepRate | ledState[2].stepRate) == 0) {
			PORTB = 0;
			LED3_OFF();
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);
			sei();
			sleep_mode();
			SET_WDIE();
			status.ticked = 1;
			status.fellAsleep = 1;
		} else {
			set_sleep_mode(SLEEP_MODE_IDLE);
			wdt_reset();
			sei();
			sleep_mode();
		}
    }
}